# Test Cuemby


## Front-end
This project implement the following tecnologies
### Develop
- angular 1.6 based in components.
- Webpack 3.1.0,
- babel
(transform-async-to-generator to support async / await, presets es2015, es2016, es2017 ).
- Guie stile angular of todd motto

## back-end
This project implement the following tecnologies
### Develop
- Webpack 3.1.0
- babel
- node 6.x.x
- for the test mocha, chai, httpchai, coverages nyc
- express


#### Install dependencies
```bash
yarn install
npm install
```
#### Run Project with all commands 
```bash
npm run app:all
```

#### build Project
```bash
npm run app:build
```

#### run test
```bash
npm run app:cover
```

#### run server
```bash
npm start
```
