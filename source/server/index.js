import http from 'http'
import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'

//module router api
import api from './api/api'

const app = express()
const server = http.createServer(app)
const port = process.env.PORT || 9001

app.use(bodyParser.json())
//Middleware implement router break points app
app.use('/api', api)
//middleware responde assets app angular
app.use(express.static('app'))


server.listen(port, () => console.log(`Server listening for http://localhost:${port}`))

//export app for test
export default app