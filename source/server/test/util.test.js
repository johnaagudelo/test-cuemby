const mocha = require('mocha')
const chai = require('chai')
const util =  require('../api/util')
const s = require('../api/students.json')

const expect = chai.expect

describe('test functions utilities', () => {
	it('should return all students', ()=>{
        const students = util.default.all()
        expect(students).to.be.instanceof(Object)
        expect(students.students.length).to.equals(6)
    })

    it('should return students with active true', ()=>{
        const students = util.default.filterStatus(true)
        expect(students).to.be.instanceof(Array)
        expect(students.length).to.equals(4)
    })

    it('should search student per id', ()=>{
        const students = util.default.filterId(101)
        expect(students).to.be.instanceof(Array)
        expect(students.length).to.equals(1)
        expect(students[0].name).to.equals("Carlos")
        expect(students[0].active).to.equals(true)
    })

    it('should calculate the average of student', ()=>{
        let student = s.students[0]
        let sum = student.grades.reduce((val, index) => val + index, 0)
        const average = sum / student.grades.length
        student.average = average.toFixed(2);
        const return_student = util.default.average(student)
        expect(return_student).to.be.instanceof(Object)
        expect(return_student).to.have.property('average')
        expect(return_student.average).to.equals(student.average)
        expect(return_student).to.equals(student)
    })

    it('should return null if not send student', ()=>{
        const return_student = util.default.average()

        expect(return_student).to.equals(null)
    })
})