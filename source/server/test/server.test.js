const mocha = require('mocha')
const chai = require('chai')
const chaiHttp = require('chai-http')
const app =  require('../')

chai.use(chaiHttp)
const expect = chai.expect

describe('/GET students', () => {
    it('it should GET all the students', (done) => {
    chai.request('http://localhost:9001')
        .get('/api/students')
        .end((err, res) => {
            expect(res).to.have.status(200);
            done()
        })
    })
})

describe('/GET students', () => {
    it('it should GET return the students with active true', (done) => {
    chai.request('http://localhost:9001')
        .get('/api/students?active=true')
        .end((err, res) => {
            expect(res).to.have.status(200);
            done()
        })
    })
})

describe('/GET/:id student', () => {
    it('it should GET one student', (done) => {
    chai.request('http://localhost:9001')
        .get('/api/student/101')
        .end((err, res) => {
            expect(res).to.have.status(200);
            done()
        })
    })
})

describe('/GET/:id student', () => {
    it('it should return state 400', (done) => {
    chai.request('http://localhost:9001')
        .get('/api/student/1000')
        .end((err, res) => {
            expect(res).to.have.status(400);
            done()
        })
    })
})