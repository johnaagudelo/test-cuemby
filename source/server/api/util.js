import students from './students.json'

/**
 * Objeto that contains the methods to operate the data of students
 */
const utilities = {
    //get all studens json
    all: function(){
        return students
    },
    //filter students json by active (true | false) 
    filterStatus: function (status) {
        return students.students.filter(student => student.active == status)
    },
    //get student per id 
    filterId: function (id) {
        return students.students.filter(student => student.id == id)
    },
    //calculate average the student
    average: function(student = null){
        if (student == null) return null 
        let grades = student.grades
        //function recursive to do the sum
        let sum_grades = grades.reduce((val, index) => val + index, 0)
        const average = sum_grades / grades.length
        //get only two decimals of the overage
        student.average = average.toFixed(2);
        return student
    }
}
export default utilities