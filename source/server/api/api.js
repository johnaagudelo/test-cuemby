import express from 'express'
import students from './students.json'
import util from './util'
const router = express.Router()

/**
 * Route for ge all students or students filter for the key "active" (true | false)
 */
router.get('/students', (req, res) => {
    let active = req.query.active || null
    active = (active == 'true')
    if(active){
        res.json({ "students": util.filterStatus(active) })
    }else{
        res.json(util.all())
    }
})

/**
 * Route get student for id
 */
router.get('/student/:id', (req, res) => {
    let id = req.params.id
    let student = util.filterId(id)
    if (student.length > 0) {
        res.json({ "student": util.average(student[0]) })
    }else{
        return res.sendStatus(400)
    }
})

export default router