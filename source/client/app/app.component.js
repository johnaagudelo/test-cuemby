import template from './app.view.html';
import './app.scss';

const appComponent = {
    template,
    restrict: 'E'
};

export default appComponent;