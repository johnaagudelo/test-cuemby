import angular from 'angular'
import uiRouter from 'angular-ui-router'
import http from './common/provider'
import Components from './components/components.module'
import AppComponent from './app.component'


angular.module('app', [
    uiRouter,
    Components
])
.config(($urlRouterProvider) => {
    'ngInject'
    $urlRouterProvider.otherwise('/students')
})
.service('httpProvider', http)
.component('app', AppComponent)
