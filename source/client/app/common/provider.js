class Http {
    constructor($http, $q) {
        'ngInject'
        this.$http = $http
        this.q = $q
        this.data = {}
    }

    /**
     * 
     * @param {* Url api for request data } url 
     */
    req(url) {
        const deferred = this.q.defer()
        this.data = {
            method: 'GET',
            url:  url,
            data: {},
        }
        this.$http(this.data).then(function (response) {
            deferred.resolve(response)
        }, function (err) {
            deferred.resolve(err)
        })
        return deferred.promise
    }

}
export default Http
