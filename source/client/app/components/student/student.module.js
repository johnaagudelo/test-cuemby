import angular from 'angular'
import uiRouter from 'angular-ui-router'
import studentComponent from './student.component'

let studentModule = angular.module('student', [
    uiRouter
])
    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';
        $urlRouterProvider.otherwise('/student');

        $stateProvider
            .state('student', {
                cache: false,
                url: '/student/:id',
                component: 'student'
            })
    })
    .component('student', studentComponent)
    .name;

export default studentModule;
