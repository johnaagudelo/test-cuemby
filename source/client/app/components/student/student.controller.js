class StudentController {
    constructor(httpProvider, $state, $scope, $stateParams) {
        'ngInject'
        this.http = httpProvider
        this.state = $state
        this.scope = $scope
        this.id = $stateParams.id
        this.student = {}
    }

    /**
     * Inicialization of component detail student
     */
    async $onInit() {
        //verification param id student else return home students
        if(this.id){
            try {
                //request students 
                const response = await this.http.req(`/api/student/${this.id}`)
                this.student = response.data.student
                this.scope.$apply()
            } catch (error) {
                console.log(error)
            }
        }else{
            this.state.go('students');
        }
    }

    /**
     * 
     * @param {*param value grade for get name class color} grade 
     */
    rangeGrade(grade){
        if(grade < 3)
            return 'red'
        if(grade >= 3 && grade < 4)
            return 'yellow'
        if(grade >= 4)
            return 'green'
    }

    /**
     * Method back home list students
     */
    back(){
        this.state.go('students');
    }
}

export default StudentController;