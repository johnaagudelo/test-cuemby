import template from './student.html'
import controller from './student.controller.js'
import './student.scss'

const studentComponent = {
    restrict: 'E',
    bindings: {},
    template,
    controller
};

export default studentComponent