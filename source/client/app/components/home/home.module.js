import angular from 'angular'
import uiRouter from 'angular-ui-router'
import homeComponent from './home.component'

let homeModule = angular.module('home', [
    uiRouter
])
    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';
        $urlRouterProvider.otherwise('/students');

        $stateProvider
            .state('students', {
                cache: false,
                url: '/students',
                component: 'home'
            })
    })
    .component('home', homeComponent)
    .name;

export default homeModule;
