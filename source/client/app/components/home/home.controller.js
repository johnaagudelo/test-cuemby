class HomeController {
    constructor(httpProvider, $state, $scope) {
        'ngInject'
        this.http = httpProvider
        this.state = $state
        this.scope = $scope
        this.students = []
    }

    /**
     * Inicialization component, call api students
     */
    async $onInit() {
        try {
            //call api with query active for get only students with active true
            const response = await this.http.req('/api/students?active=true')
            this.students = response.data.students
            this.scope.$apply()
        } catch (error) {
            console.log(error)
        }
    }

    /**
     * 
     * @param { identifier student for go detail } _id 
     */
    detalle(_id) {
        this.state.go('student', { id: _id })
    }
}

export default HomeController;