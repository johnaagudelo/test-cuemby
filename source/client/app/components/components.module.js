import angular from 'angular'
import Home from './home/home.module'
import Student from './student/student.module'
const componentModule = angular.module('app.components', [ Home, Student ]).name;
export default componentModule
