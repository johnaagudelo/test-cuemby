'use strict'
const path = require('path')
let CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = {
    entry: ['babel-polyfill', './source/client/app/app.module.js'],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../app'),
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: './source/client/index.html',
                to: 'index.html'
            }
        ]),
    ],
    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /(node_modules)/,
                query: {
                    presets: ['es2015', 'es2016', 'es2017']
                }
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.(scss|sass)$/,
                loader: 'style-loader!css-loader!sass-loader'
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }
        ]
    },
    target: 'web'
}