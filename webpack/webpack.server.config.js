const path = require('path');

module.exports = {
    entry: './source/server/',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, '../server'),
    },
    module: {
        loaders: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /(node_modules)/,
                query: {
                    presets: ['es2016', 'es2017']
                }
            }
        ]
    },
    target: 'node'
}